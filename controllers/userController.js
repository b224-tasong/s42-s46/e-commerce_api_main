const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// ========== Register User =================================

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)

	})
	
	return newUser.save().then((result, error) => {
		if (error) {
			return false
		}else{
			return "You have successfully registered your account"
		}
	})
};


//================= User Login ===============================
 
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if (result == null) {
			return false
		}else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			if (isPasswordCorrect) {
				return {tokenAccess: auth.createAccessToken(result)}
			}else{
				return false
			}
		}	
	})
};


//================= Check Duplicate Emails ===============================
module.exports.checkDuplicateEmail = (reqBody, res) => {

	return User.find({email: reqBody.email}).then(result => {
			
			// The "find" method returns a record if a match is found
			if(result.length > 0) {
				return true
			// No duplicate email found
			// The user is not yet registered in the database
			} else if (result.length == 0) {
				return false
			}
		})
		
};


//==================== Create Checkout Order ==========================

module.exports.checkOutProduct = async (userData, prodData) => {
	let user_id = userData.id;
	let total;


	let isUserUpdated = await User.findById(user_id).then(userResult => {

		if (userResult == null) {
			return false;
		}



		return Product.findById(prodData.productId).then(getProdDataResult => {

			if (getProdDataResult.isActive === false) {
				return false
			}else{
				userResult.orderedProduct.push({
					products: [
							{
								productId: prodData.productId,
								productName: getProdDataResult.name,
								quantity: prodData.quantity
							}
						],
					totalAmount: total = getProdDataResult.price * prodData.quantity
				});
				return userResult.save().then((result, error) => {
					if (error) {
						return false
					}else{
						return true
					}
				})
			}	
		})
	});


	let isProductUpdated = await Product.findById(prodData.productId).then(prodResult => {

		if (prodResult == null) {
			return false;
		}
		return User.findById(userData.id).then(userResult => {
			
			let orderId = userResult.orderedProduct[userResult.orderedProduct.length-1]._id
			
			if (prodResult.isActive) {
				prodResult.userOrder.push({
					userId: userData.id,
					orderId: orderId
				});


				return prodResult.save().then((result, error) => {
					if (error) {
						return false
					}else{
						return true
					}
				})
			}else{
				return false
			}
		})
	});


	if (isUserUpdated && isProductUpdated) {
		return "User has already placed an order successfully"
	}else{
		return "Out of Stock!"
	}                                              
};




//==================== Retrieve User Details ==========================

module.exports.retrieveUserDetails = (userData) => {

	return User.findById(userData.id).then(result => {
		let userFname = result.firstName;
		let userLname = result.lastName;
		let email = result.email;
		let pass = result.password;
		let userRole = result.isAdmin;
		return `Name: ${userFname} ${userLname}\n 
				Email: ${email}\n
				Password: ${pass}\n
				isAdmin: ${userRole}`
	});
};










//==================== Set User as an Admin ==========================

module.exports.setUserAsAdmin = (reqParams, reqBody) => {
	let updateUserRole = {

		isAdmin: reqBody.isAdmin

	}

	return User.findByIdAndUpdate(reqParams.id, updateUserRole).then((result, error) => {

		if (error) {
			return	false
		}else{
			return result
		}

	});
};




//==================== Retrieve All Orders ==========================

module.exports.getAllOrders = () => {

	return User.find({}).then(result => {
		let allOrders = [];
		result.forEach(user => {
			user.orderedProduct.forEach(order => {
				let orderDetails = {
					"products": order.products,
					"totalAmount": order.totalAmount,
					"purchasedOn": order.purchasedOn
				}
				allOrders.push(orderDetails);
			});
		});
		return allOrders;
	})
}





//==================== Retrieve Authenticated User Order ==========================

module.exports.retrieveUserOrderDetails = (userData) => {

	return User.findById(userData.id).then(result => {

		return `Ordered Details: \n
				${result.orderedProduct}\n`
	});
};



//==================== Add to Cart ==========================
let prodArray = [];
let userArray = [];
module.exports.addToCart = (prodData) => {
	

	return Product.findById(prodData.productId).select("name price descr").then(prodResult => {
		
		
		if (prodResult.isActive == false) {
			return "Out of Stock!"
		}else{
			prodResult.price *= prodData.quantity
			prodArray.push({prodResult});

			return prodArray
		}
	})	
};


