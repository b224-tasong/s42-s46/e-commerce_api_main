
const Product = require("../models/Product");
const express = require('express');
const bcrypt = require("bcrypt");
const auth = require("../auth");





//================= Create Product ===============================
module.exports.createProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})
	return newProduct.save().then((result, error) =>{
		if (error) {
			return false
		}else{
			return true
		}
	})
};


//================= Update Product Info ===============================

module.exports.updateProductInfo = (reqParams, reqBody) => {

	let updateProductInfo = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.id, updateProductInfo).then((result, error) =>{
		if (error) {
			return false
		}else{
			return true
		}
	})
};





//================= ARCHIVE Product Info ===============================

module.exports.ArchiveProduct = (reqParams, reqBody) => {
	let updateIsActive = {

		isActive: reqBody.isActive
	}
	
	return Product.findByIdAndUpdate(reqParams.id, updateIsActive).then((result, error) =>{
		if (error) {
			return false
		}else{
			return true
		}
	})
};







//================= Delete Product Info ===============================
// module.exports.deleteProdInfo = (prodId) => {

// 	return Product.findByIdAndDelete(prodId).then((result, error) =>{
// 		if (error) {
// 			return false
// 		}else{
// 			return true
// 		}
// 	})
// };

//================= Retrieve all products by admin ===================

module.exports.adminGetAllProduct = () => {
	return Product.find({}).then(result => {
		return result
	})
};





// ================ Get Specific Product Info ==========================

module.exports.getSpecificProduct = (prodId) => {

	return Product.findById(prodId).then(result => {

		return result
	})
};



//================= GET all active products ===============================
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
};















