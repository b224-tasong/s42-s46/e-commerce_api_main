const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// ========== Register User =================================

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
});



//================= User Login ===============================

router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(result => res.send(result))
	
});



//================= Check Duplicate Emails ===============================
router.post("/checkEmail", (req, res) =>{
	// console.log(req.body)
	userController.checkDuplicateEmail(req.body, res).then(result => res.send(result))
});


//==================== Create Checkout Order ==========================

router.post("/order", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)
	
	let prodData = {
		productId: req.body.productId,
		quantity: req.body.quantity
	}

	if (userData.isAdmin == false) {
		console.log("Please select a product to checkout")
		userController.checkOutProduct(userData, prodData).then(result => res.send(result))
	}else{
		res.send({auth: "Please use a user account for this function"})
	}
	
	
});


//==================== Retrieve User Details ==========================

router.get("/details", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin == false) {

		userController.retrieveUserDetails(userData).then(result => res.send(result))	
	}else{
		res.send({auth: "Please use a user account for this function"})
	}
});








//==================== Set User as an Admin ==========================

router.patch("/setAdmin/:id", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin == false) {

		res.send({auth: "You are not an admin"})
	
	}else{

		userController.setUserAsAdmin(req.params, req.body).then(result => res.send(result))
	}
});




//==================== Retrieve All Orders ==========================

router.get("/allOrders", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin == false) {

		res.send({auth: "You are not an admin"})
	
	}else{

		userController.getAllOrders().then(result => res.send(result))
	}
});


//==================== Retrieve Authenticated User Order ==========================

router.get("/AuthenticatedUserDetails", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin == false) {

		userController.retrieveUserOrderDetails(userData).then(result => res.send(result))	
	}else{
		res.send({auth: "Please use a user account for this function"})
	}
});


//==================== Add to Cart ==========================

router.post("/cart", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	let prodData = {
		productId: req.body.productId,
		quantity: req.body.quantity
	}

	if (userData.isAdmin == false) {

		userController.addToCart(prodData).then(result => res.send(result))	
	}else{
		res.send({auth: "Please use a user account for this function"})
	}
});




module.exports = router ; 
